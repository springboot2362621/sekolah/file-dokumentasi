
--insert ROLE
insert into sekolah.public.mst_roles("uuid","name" ,"is_active","created_at",updated_at) values
('0247f6be-aa5c-4325-8617-bac2c562f0a2','Admin',true,NOW(),NOW()),
('8683f057-d64c-47ae-a217-189e647a2b56','Guru',true,NOW(),NOW()),
('951bda26-b372-42cb-b240-03be90f7a7c3','Siswa',true,NOW(),NOW())


--insert USER
insert into sekolah.public.mst_users ("uuid","is_active","created_at","updated_at","email","is_login",
"name","password","phone_number","role_id") values
('e1fe9ff5-bbfc-423c-8168-c9cb43ab5956',true,NOW(),NOW(),'test1@gmail.com',false,'test1','12345','087233345343','0247f6be-aa5c-4325-8617-bac2c562f0a2'),
('4cfe3e21-f227-4929-807a-5d85908471fd',true,NOW(),NOW(),'test2@gmail.com',false,'test2','12345','087233354532','0247f6be-aa5c-4325-8617-bac2c562f0a2')